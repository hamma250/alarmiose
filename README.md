[![pipeline status](https://gitlab.com/hamma250/alarmiose/badges/master/pipeline.svg)](https://gitlab.com/hamma250/alarmiose/commits/master)

---

Website for AlarmioSE which downloads artifacts with a private token from the private repository and then publishes it.
You can find it here: https://hamma250.gitlab.io/alarmiose/

---