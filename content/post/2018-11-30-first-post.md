---
title: Release of Alarmio Special Edition
date: 2018-11-30
draft: false
---

First public release of Alarmio SE. Its only a debug apk, so don't download it.

[Download Alarmio][1]

[1]: {{< ref "/downloads/download.md" >}}