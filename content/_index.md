## Alarmio Special Edition
The the alarm clock to use for safe partying

For downloads go [here][1]

[1]: {{< ref "downloads/download.md" >}}

If you have any issues feel free to ask for [help here!][2]

[2]: https://gitlab.com/hamma250/alarmiose/issues

original creator: https://github.com/fennifith/Alarmio