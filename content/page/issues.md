---
title: Issues
subtitle: 
comments: false
---
## If you need help ask
You can just send an email here and will get some support: {{< email "incoming+hamma250/alarmiose@incoming.gitlab.com" "Email here" >}}

Additionally you can open a user account at gitlab.com or sign in using your preferred social account.
There you will also find solutions to similar problems.

[Gitlab Issue Tracker][2]

[2]: https://gitlab.com/hamma250/alarmiose/issues